




@startuml
start
:Надслати запит для купівлі квитка
:Вибрати напрямок автобуса,дату
:Вибрати місце
    repeat :Надіслати запит з оплатою квитку;
    backward :Отримання помилки;
:Очікувати обробку запиту 3 с
    repeat while (Квиток оплачено?) is (Ні) not (Так)
:Надіслати запит на реєстрацію; 
:Отримання повідомлення про успішно куплений квиток;

  stop
@enduml